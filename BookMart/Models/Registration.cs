﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace BookMart.Models
{
    public partial class Registration
    {
        public int UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",ErrorMessage ="Please Enter Valid Email")]
        public string Email { get; set; }
       
        public string Contact { get; set; }
        [Required]
        [StringLength(20,ErrorMessage ="Username not be exceed")]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password")]
        public string ResetPassword { get; set; }
    }
}
