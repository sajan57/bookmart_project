﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BookMart.Models
{
    public partial class Books
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public string Introduction { get; set; }
        public string Rating { get; set; }
        public string BookCatagory { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public string ImageUrl { get; set; }

    }
}
