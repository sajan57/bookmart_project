﻿using BookMart.Data;
using BookMart.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace BookMart.Controllers
{
    public class LoginController : Controller
    {
        private readonly BookMartContext _context;
        public LoginController(BookMartContext context)
        {
            _context = context;
        }


        public IActionResult Login2()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login2(Registration registration)
        {
            var user = _context.Registration.Any(x => x.UserName == registration.UserName && x.Password == registration.Password);
            if (user)
            {
                return RedirectToAction("Index", "Home");
            }
            return View(nameof(Login2));
        }

    }
}

    




